import sys, requests

def main(dict):
    url = 'https://chatbot-test-04.oit.duke.edu/webhook'
    r = requests.post(url, json=dict)
    print(r.status_code)
    return { 'message': str(dict.keys()) }
