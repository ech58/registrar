import sys
import smtplib
from socket import gaierror
def send_email(dict):
    port = 587
    smtp_server = "smtp.office365.com"
    login = "registrarbot@outlook.com" 
    password = "Chat-bot"
    
    # the sender’s and receiver’s email addresses
    sender = "registrarbot@outlook.com"
    receiver = "mhnegus@gmail.com"
    
    # type your message: use two newlines (\n) to separate the subject from the message body, and use 'f' to  automatically insert variables in the text
    message = f"""\
    Subject: Hi from IBM\
    To: {receiver}\
    From: {sender}

    This is my first message with Python.
    {dict}
    """
    try:
        #send your message with credentials specified above
        with smtplib.SMTP(smtp_server, port) as server:
            server.starttls()
            server.login(login, password)
            server.sendmail(sender, receiver, message)
        # tell the script to report if your message was sent or which errors need to be fixed
        print('Sent', file=sys.stderr)
        
    except (gaierror, ConnectionRefusedError):
        print('Failed to connect to the server. Bad connection settings?', file=sys.stderr)
    except smtplib.SMTPServerDisconnected:
        print('Failed to connect to the server. Wrong user/password?', file=sys.stderr)
    except smtplib.SMTPException as e:
        print('SMTP error occurred: ' + str(e), file=sys.stderr)
    return { 'message': 'Hello world' }