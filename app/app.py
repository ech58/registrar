from flask import Flask, request, Response, render_template, jsonify
from typing import List, Dict
import mysql.connector
from mysql.connector import Error
import json
from connection import * 
import time
import sys
import smtplib
from myemail import * 

app = Flask(__name__)

@app.route('/')
def index():
    #Timestamp for log file  
    """
    ts = time.gmtime()
    data = request.args
    str_data = json.dumps(data)
    f = open("log.txt", "a")
    f.write(time.strftime("%Y-%m-%d %H:%M:%S", ts) + " " + request.remote_addr + " \t" + str_data + "\n")
    print("after writing", file=sys.stderr)
    f.close()
    """
    return render_template("my_template.html")
    
#webhook     
@app.route('/webhook', methods=['POST', 'GET'])
def respond():
    #parsing JSON object and saving values as variables 
    details = request.get_json()
    question = details.get("question")
    print(question, file=sys.stderr)
    answer = "------" #hard-coded for now
    intent = "------" #hard-coded for now 
    print(details, file=sys.stderr)
    #print(question, file=sys.stderr)


    #insert data as a row in questions table 
    if not (question == None or answer == None or intent == None):
        insert_statement = "INSERT INTO interactions (question, answer, intent) VALUES (%s, %s, %s)"
        example_values = (str(question), str(answer), str(intent)) 
        connection = create_server_connection("db", "root", "4baUTUgYeCrQKaS9", "sample_qna") # Connect to the Database
        execute_query_insert(connection, insert_statement, example_values) # Execute our defined query

    #example JSON object for IBM bot
    example_dict = {"message" : "Hello, world!"}
    obj = jsonify(example_dict)
    print("Webhook called", file=sys.stderr)
    return obj

    #READ data from table 
    #q1 = """
    #SELECT *
    #FROM interactions;
    #"""
    """ connection = create_server_connection("db", "root", "4baUTUgYeCrQKaS9", "sample_qna")
    results = read_query(connection, q1)
    for result in results:
        print(result, file=sys.stderr) """
    
    
    #UPDATE a record 
    #Ex: changing intent for a specific question 
    #update = """
    #UPDATE interactions 
    #SET intent = 'form requests' 
    #WHERE question = 'How do I request a trancript?';
    #"""
    ##WHERE uniquely identifies which record/records to update 
    #connection = create_server_connection("db", "root", "4baUTUgYeCrQKaS9", "sample_qna")
    #execute_query(connection, update)
    
    #DELETE a record 
    #deletes all instances of the question in the table 
    #delete_question = """
    #DELETE FROM interactions 
    #WHERE question = 'bar';
    #"""
    #connection = create_server_connection("db", "root", "4baUTUgYeCrQKaS9", "sample_qna")
    #execute_query(connection, delete_question)
    
    #return Response(status=200)

@app.route('/database')
def display_database():
    connection = create_server_connection("db", "root", "4baUTUgYeCrQKaS9", "sample_qna") # Connect to the Database
    cursor = connection.cursor() 
    cursor.execute("select * from interactions")
    data = cursor.fetchall() #data from database 
    return render_template("db_template.html", value=data)  


if __name__ == '__main__':
    app.run(host='0.0.0.0')
